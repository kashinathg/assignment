package assignment.model;

/**
 * Created by Kashi.
 */
public class InvalidTickerException extends Exception {

    public InvalidTickerException(){super();}

    public InvalidTickerException(String message){
        super(message);
    }
}
